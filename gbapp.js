var path = require("path");
var express = require("express");
var logger = require("morgan");
var bodyParser = require("body-parser");

var app = express();  // make express app
var server = require('http').createServer(app);
app.use(express.static(__dirname + '/assets')) // inject app into the server

// 1 set up the view engine
app.set("views", path.resolve(__dirname, "views")) // path to views
app.set("view engine", "ejs") // specify our view engine

// 2 create an array to manage our entries
var entries = []
app.locals.entries = entries // now entries can be accessed in .ejs files

// 3 set up an http request logger to log every request automagically
app.use(logger("dev"))     // app.use() establishes middleware functions
app.use(bodyParser.urlencoded({ extended: false }))

// 4 handle http GET requests (default & /new-entry)
app.get("/", function(request, response){
  response.sendFile(__dirname+"/views/home.html")
});
app.get("/about", function(request, response){
  response.sendFile(__dirname+"/views/home.html")
});
app.get("/Calculation", function(request, response){
  response.sendFile(__dirname+"/views/Calculation.html")
});
app.get("/Contact", function(request, response){
  response.sendFile(__dirname+"/views/Contact.html")
});
app.get("/GuestBook", function (request, response) {
  response.render("index")
})
app.get("/new-entry", function (request, response) {
  response.render("new-entry")
})

// 5 handle an http POST request to the new-entry URI 
app.post("/Contact",function(req,res){
  var api_key = 'key-14f1c347745f76e84e8ce83b2eba48fa';
  var domain = 'sandboxb53dee3e86ed4780889afe5f419bdc3c.mailgun.org';
  var mailgun = require('mailgun-js')({apiKey: api_key, domain: domain});
   
  var data = {
    from: 'Excited User <postmaster@sandboxb53dee3e86ed4780889afe5f419bdc3c.mailgun.org>',
    to: 'adityamalireddy@gmail.com',
    subject: req.body.name1 +" sent you a message",
    text: "\nEmail ID :"+req.body.mail+"\nComments : "+req.body.comment
  };
   
  mailgun.messages().send(data, function (error, body) {
    console.log(body);
    console.log(error);
    if(!error){
      res.send("Mail Sent");
    }
      else
          res.send("Mail not sent");
    })
});
app.post("/new-entry", function (request, response) {
  if (!request.body.title || !request.body.body) {
    response.status(400).send("Entries must have a title and a body.")
    return
  }
  entries.push({  // store it
    title: request.body.title,
    content: request.body.body,
    published: new Date()
  })
  response.redirect("/GuestBook")  // where to go next? Let's go to the home page :)
})

// if we get a 404 status, render our 404.ejs view
app.use(function (request, response) {
  response.status(404).render("404")
})


// Listen for an application request on port 8081
server.listen(8081, function () {
  console.log('Guestbook app listening on http://127.0.0.1:8081/');
});
